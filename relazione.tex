\documentclass{article}
\usepackage[italian]{babel}
\usepackage[a4paper,top=2cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subcaption}
\usepackage{braket}
\usepackage{cancel}
\usepackage{graphicx}
\usepackage{float}
\usepackage[dvipsnames]{xcolor}
\usepackage[pdftex,
            pdfauthor={Diana Bucca},
            pdftitle={Scheduling real-time nel Coderbot},
            colorlinks=true,
            allcolors=BlueViolet]{hyperref}
\usepackage{svg}
\usepackage{tabularray, caption}
\usepackage{physics}

\usepackage{listings}
\usepackage{color}
\usepackage{dirtree}
\usepackage{fontawesome}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=C,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=fixed,
  basicstyle={\small\ttfamily},
  numbers=left,
  firstnumber=auto,
  numberstyle=\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\newcommand\coloricon[2]{{\color{#1}#2}}

\author{Diana Bucca}
\title{Scheduling real-time nel Coderbot}
\date{A.A. 2023/24}
\makeindex

\begin{document}

\maketitle
\tableofcontents

\newpage

\section*{Introduzione}

Il coderbot è un robot basato sul Raspberry Pi ideato per insegnare le basi della programmazione e della robotica ai bambini. è provvisto di due motori alimentati tramite un ponte H e controllati usando il PWM.\@
Il coderbot sarebbe dotato di svariate periferiche, di cui però non si farà uso e che quindi sono state rimosse. La programmazione è stata fatta in C usando la libreria \lstinline{pigpio}, che permette di controllare i pin General Purpose I/O.
La lettura della velocità è fatta attraverso due encoder in quadratura, uno per ruota. Il codice del progetto può essere consultato a \href{https://gitlab.com/D_B_0/real-time-coderbot}{questo link}

\subsection*{Obiettivi}

\begin{itemize}
	\item[-] Lettura encoder
	\item[-] Implementazione del control loop delle ruote in modalità indipendente e determinazione dei parametri
	\item[-] Implementazione del control loop incrociato
	\item[-] Calcolo di posizione cartesiana e orientamento
	\item[-] Implementazione del control loop che permette di seguire il percorso indicato dall'utente
	\item[-] Strutturazione del codice secondo una architettura real-time basata sullo scheduling EDF (earliest deadline first)
\end{itemize}

\section{Analisi del contesto}

\subsection{Raspberry Pi e libreria pigpio}

Il Raspberry Pi è un single board computer che ospita Rasbian, una distribuzione di Linux basata su Debian, e ha una cpu con architettura ARM.\@ Nel coderbot i pin GPIO sono usati esclusivamente dallo shield, a cui sono connessi encoder e motori. C'è una mappatura 1 a 1 tra pin dello shield e del pi.

La libreria \lstinline{pigpio} viene usata per l'interfaccia GPIO.\@ Di particolare interesse sono le seguenti funzioni
\begin{itemize}
	\item[-] \lstinline{int gpioInitialize(void)}, necessaria prima di ogni operazione GPIO % chktex 36
	\item[-] \lstinline{void gpioTerminate(void)}, termina la libreria % chktex 36
	\item[-] \lstinline{int gpioPWM(unsigned int user_gpio, unsigned int dutycycle)}, imposta il pin \lstinline{user_gpio} \\ con un PWM con duty cycle \lstinline{dutycycle}, compreso tra \lstinline{0} e \lstinline{range} (\lstinline{255} di default) % chktex 36
	\item[-] \lstinline|int gpioSetISRFuncEx(unsigned int gpio, unsigned int edge, int timeout,| \\ \lstinline|gpioISRFuncEx_t f, void *userdata)|, imposta la funzione \lstinline{f} come callback quando il segnale sul pin \lstinline{gpio} cambia, a seconda del valore di \lstinline{edge}, passandogli \lstinline{userdata} % chktex 36
	\item[-] \lstinline{uint32_t gpioTick(void)}, restituisce il numero di microsecondi dall'accensione % chktex 36
\end{itemize}

Si è fatto uso di alcune struct e utility della libreria \lstinline{libcoderbot}.\@ \lstinline{cbdef.h} fornisce enum per i pin delle periferiche e per la direzione degli encoder, \lstinline{encoder.h} una struttura per gli encoder contenente informazioni rilevati più un campo custom, utilizzato per tenere traccia del timestamp a cui è arrivato l'ultimo interrupt, e \lstinline{motor.h} una struttura e delle funzioni per il controllo dei motori.

\subsection{Scheduler}

Per lo scheduling real-time si è utilizzato uno scheduler messo a disposizione da Linux, in particolare quello chiamato \lstinline{SCHED_DEADLINE}, che usa l'algoritmo Earliest Deadline First, il cui principio è in ogni istante il task con la deadline più vicina è quello in esecuzione. Questo è un algoritmo \textit{preemptive}, quindi i task possono essere interrotti durante la loro esecuzione, se un task a priorità più alta, ovvero con deadline assoluta più vicina, viene rilasciato. Nell'applicazione ci sono alcune risorse in condivisione, il che necessita di un metodo di sincronizzazione tra i task, individuato nei \lstinline{pthread_mutex_t} forniti dalla libreria \lstinline{pthread.h} di Linux. Inoltre i mutex sono impostati per usare il \textit{priority inheritance}, stratagemma che permette di evitare problemi come il \textit{priority inversion}, anche se la nostra applicazione è piuttosto semplice, avendo solo due task, e quindi non sarebbe necessario.

\section{Implementazione}

\subsection{Struttura del codice}

\dirtree{%
	.1 . % chktex 26
	.2 \coloricon{ForestGreen}{\faFolderO} src.
	.3 \coloricon{RoyalBlue}{\faFileCodeO} ISRs.c.
	.3 \coloricon{RoyalBlue}{\faFileCodeO} main.c.
	.3 \coloricon{RoyalBlue}{\faFileCodeO} odometry.c.
	.3 \coloricon{RoyalBlue}{\faFileCodeO} pid.c.
	.3 \coloricon{RoyalBlue}{\faFileCodeO} ring\_buffer.c.
	.2 \coloricon{ForestGreen}{\faFolderO} include.
	.3 \coloricon{RoyalBlue}{\faFileTextO} constants.h.
	.3 \coloricon{RoyalBlue}{\faFileTextO} ISRs.h.
	.3 \coloricon{RoyalBlue}{\faFileTextO} odometry.h.
	.3 \coloricon{RoyalBlue}{\faFileTextO} pid.h.
	.3 \coloricon{RoyalBlue}{\faFileTextO} ring\_buffer.h.
	.3 \coloricon{RoyalBlue}{\faFileTextO} sched\_parameters.h.
	.3 \coloricon{RoyalBlue}{\faFileTextO} wheel\_constants.h.
}

Il codice è stato diviso in vari file sorgente e header, per comodità e per separazione degli interessi.
L'entry point dell'applicazione si trova in \lstinline{main.c}, dove sono anche definiti i due task.\ \lstinline{odometry.c} e \lstinline{odometry.h} sono stati scritti dai colleghi di robotica.

\subsection{Ring Buffer}

I file \lstinline{ring_buffer.c} e \lstinline{.h} contengono una semplice implementazione di un buffer circolare, utile in vari posti nel progetto, atto a contenere un numero arbitrario di \lstinline{float}.

\begin{lstlisting}[title={Snippet da \lstinline|ring_buffer.c|}]
void buffer_init(mBuffer_t* buf, int dim) {
  buf->dim = dim;
  buf->index = 0;
  buf->buffer = (float*)malloc(dim * sizeof(float));
  memset(buf->buffer, 0.0f, dim * sizeof(float));
}

void buffer_free(mBuffer_t* buf) {
  buf->dim = 0;
  buf->index = 0;
  free((void*)buf->buffer);
}

void buffer_push(mBuffer_t* buf, float val) {
  buf->index = (buf->index + 1) % buf->dim;
  buf->buffer[buf->index] = val;
}

float buffer_top(const mBuffer_t* buf) {
  return buf->buffer[buf->index];
}

float buffer_bottom(const mBuffer_t* buf) {
  return buf->buffer[(buf->index + 1) % buf->dim];
}

float buffer_at(const mBuffer_t* buf, int i) {
  return buf->buffer[(buf->index + buf->dim - i) % buf->dim];
}
\end{lstlisting}

Per un utilizzo corretto della struttura dati è necessario chiamare \lstinline{buffer_init} prima di poter chiamare qualsiasi altra funzione del buffer e \lstinline|buffer_free| quando si è finito di utilizzarla. È possibile notare che non è stata implementata alcuna funzionalità di rimozione, dato che ai fini applicativi non è servito.

Il funzionamento è analogo a quello di uno stack, in cui l'ultimo elemento inserito si trova in cima (viene restituito da \lstinline|buffer_top|), con la possibilità di accesso arbitrario.

\subsection{Program flow}

Per facilitare la comprensione della logica di esecuzione è esposto il flow del programma di seguito. All'inizio viene chiamata \lstinline|init|, che
\begin{enumerate}
	\item inizializza i GPIO
	\item registra le ISRs degli encoder
	\item inizializza i buffer globali
	\item crea il mutex
\end{enumerate}
se una di queste operazioni fallisce, il programma stampa un messaggio di errore e termina.

Vengono poi creati i thread dei task, che iniziano subito l'esecuzione. Il main thread a questo punto joina la \lstinline|odo_task|, aspettando la sua terminazione, per poi cancellare la \lstinline|ticks_task| e chiudere il programma. Alla chiusura del programma, a prescindere da come essa avviene, viene chiamata \lstinline|terminate|, che effettua un reset dei GPIO e rilascia le risorse acquisite in \lstinline|init|, compreso il mutex. Inoltre, se il logging è abilitato, è a questo punto che vengono scritti i log file in formato \textit{csv}.

L'unica condizione di uscita dell'\lstinline|odo_task| si trova nel \lstinline|ctrl_c_handler|, infatti, come visto di seguito, i task sono dei cicli infiniti che quindi devono essere terminati attraverso un \lstinline|break|.

\begin{lstlisting}[title=Snippet da \lstinline|main.c|]
int terminate_flag = 0;

void ctrl_c_handler(int _sig) {
  if (terminate_flag != 0) {
    fputs("control c pressed again, killing program\n", stderr);
    exit(EXIT_FAILURE);
  }
  fputs("control c pressed!\n", stderr);
  terminate_flag = 1;
}
\end{lstlisting}

Questa funzione viene chiamata quando uno tra \lstinline|SIGINT|, \lstinline|SIGTERM| o \lstinline|SIGKILL| interrompono il programma e imposta un flag che viene poi letto da \lstinline|odo_task|. Per sicurezza, nel caso arrivino due segnali di seguito e il programma non sia ancora terminato gracefully, allora viene terminato forzatamente.

\subsection{Tasks}

Sono presenti due task: \lstinline|odo_task| e \lstinline|ticks_task|.\ \lstinline|odo_task| è responsabile di calcolare le velocità delle ruote, calcolare l'odometria ed eseguire i control loop, mentre \lstinline|ticks_task| ha il compito di recuperare i dati acquisiti dalle ISRs e inserirli nelle apposite strutture. Suddette strutture sono delle istanze di \lstinline|struct encoder_history|, che a loro volta contengono un ring buffer per i tick e uno per i timestamp, ne esiste una per encoder e sono protette da un mutex.

I parametri dei task sono i seguenti

\begin{lstlisting}[title=Snippet da \lstinline|sched_parameters.h|]
#define TICKS_WCET 10
#define ODO_WCET 80

#define TICKS_RUNTIME TICKS_WCET * NSEC_PER_MSEC
#define TICKS_PERIOD 2 * TICKS_WCET * NSEC_PER_MSEC
#define TICKS_DEADLINE (2 * TICKS_WCET - 1) * NSEC_PER_MSEC

#define ODO_RUNTIME ODO_WCET * NSEC_PER_MSEC
#define ODO_PERIOD 2 * ODO_WCET * NSEC_PER_MSEC
#define ODO_DEADLINE (2 * ODO_WCET - 1) * NSEC_PER_MSEC
\end{lstlisting}

La stima del worst case execution time è stata fatta eseguendo il programma con il logging abilitato e prendendo il massimo degli execution time, escluso il primo tempo che risulta essere sempre molto maggiore di tutti gli altri.

Per garantire la schedulabilità il \lstinline|runtime| di ciascun task è impostato come il suo WCET, il \lstinline|period| come due volte il WCET, per avere un utilization factor $U_i=\flatfrac12$, e la \lstinline|deadline| appena minore del periodo, dato che non possono essere messe uguali. Con questi parametri possiamo garantire la schedulabilità con EDF dato che

\begin{equation*}
	\sum_{i=1}^2U_i=U_1+U_2=\frac12+\frac12=1\leq1
\end{equation*}

Il WCET è molto maggiore dell'execution time medio, che è circa $26$ per la \lstinline|odo_task| e $2$ \lstinline|ticks_task|.

\subsubsection{ticks\_task}

\begin{lstlisting}[title=Snippet da \lstinline|main.c|]
void* ticks_task(void* _arg) {
  sched_attr_t attr = {.size = sizeof(attr),
                        .sched_flags = 0 | SCHED_FLAG_DL_OVERRUN,
                        .sched_policy = SCHED_DEADLINE,
                        .sched_runtime = TICKS_RUNTIME,     // ns
                        .sched_period = TICKS_PERIOD,       // ns
                        .sched_deadline = TICKS_DEADLINE};  // ns
  // Register signal handler
  (void)signal(SIGXCPU, deadline_missed_handler);
  (void)signal(SIGINT, ctrl_c_handler);
  (void)signal(SIGTERM, ctrl_c_handler);
  (void)signal(SIGKILL, ctrl_c_handler);
  ASSERT(sched_setattr(0, &attr, 0) != 0, "ticks_task: sched_setattr")
\end{lstlisting}

Il task inizia impostando i suoi vari attributi e gli handler per i segnali di sistema. In particolare il segnale \lstinline|SIGXCPU|, che indica che una deadline è stata mancata, chiama una funzione che termina il programma, invece i segnali \lstinline|SIGINT|, \lstinline|SIGTERM| e \lstinline|SIGKILL| chiamano un'altra funzione che cerca di terminare gracefully l'applicazione.

Dopo di che comincia il vero e proprio task, ovvero la parte periodica.

\begin{lstlisting}[name=ticks_task,firstnumber=last]
  for (;;) {
#ifdef LOG_WCET
    uint32_t start_time = gpioTick();
#endif
\end{lstlisting}

Qui si ha una parte della funzionalità di logging dei tempi di esecuzione, utile a stimare il WCET.\@

\begin{lstlisting}[name=ticks_task,firstnumber=last]
    int res = pthread_mutex_trylock(&history_mutex);
    if (res != 0) {
      if (errno == EBUSY) {
        // The mutex was busy, yielding...
        sched_yield();
      } else {
        perror("ticks_task: pthread_mutex_trylock");
        printf("pthread_mutex_trylock return value: %d\n", res);
        exit(EXIT_FAILURE);
      }
      continue;
    }
    buffer_push(&history_left.ticks_buf, (float)enc_left.ticks);
    buffer_push(&history_left.timestamps_buf, (float)timestamp_left);
    buffer_push(&history_right.ticks_buf, (float)enc_right.ticks);
    buffer_push(&history_right.timestamps_buf, (float)timestamp_right);
    ASSERT(pthread_mutex_unlock(&history_mutex) != 0,
            "ticks_task: pthread_mutex_unlock")
\end{lstlisting}

Questa è la sezione critica del codice, dove si va a operare sulle history degli encoder, per inserire gli ultimi dati ottenuti.

\begin{lstlisting}[name=ticks_task,firstnumber=last]
#ifdef LOG_WCET
    uint32_t end_time = gpioTick();
    if (ticks_wcet_idx < WCET_LOG_SIZE) {
      ticks_wcet[ticks_wcet_idx] = end_time - start_time;
      ticks_wcet_idx++;
    }
#endif
\end{lstlisting}

Conclusione del logging.

\begin{lstlisting}[name=ticks_task,firstnumber=last]
    pthread_testcancel();
    sched_yield();
  }
  pthread_exit(NULL);
  return NULL;
}
\end{lstlisting}

Dopo di che si verifica la cancellazione del thread, che avviene a completazione del programma, e nel caso non sia stato cancellato si cede esecuzione al sistema operativo.

\subsubsection{odo\_task}

\begin{lstlisting}[title=Snippet da \lstinline|main.c|]
void* odo_task(void* _arg) {
  sched_attr_t attr = {.size = sizeof(attr),
                       .sched_flags = 0 | SCHED_FLAG_DL_OVERRUN,
                       .sched_policy = SCHED_DEADLINE,
                       .sched_runtime = ODO_RUNTIME,     // ns
                       .sched_period = ODO_PERIOD,       // ns
                       .sched_deadline = ODO_DEADLINE};  // ns
  // Register signal handlers
  (void)signal(SIGXCPU, deadline_missed_handler);
  (void)signal(SIGINT, ctrl_c_handler);
  (void)signal(SIGTERM, ctrl_c_handler);
  (void)signal(SIGKILL, ctrl_c_handler);
  ASSERT(sched_setattr(0, &attr, 0) != 0, "odo_task: sched_setattr")
\end{lstlisting}

Gli attributi vengono impostati come nel task precedente.

\begin{lstlisting}[name=ticks_task,firstnumber=last]
  encoder_history_t history_left_local = {0};
  buffer_init(&history_left_local.ticks_buf, SNAPSHOT_BUFFER_DIM_LEFT);
  buffer_init(&history_left_local.timestamps_buf, SNAPSHOT_BUFFER_DIM_LEFT);

  encoder_history_t history_right_local = {0};
  buffer_init(&history_right_local.ticks_buf, SNAPSHOT_BUFFER_DIM_RIGHT);
  buffer_init(&history_right_local.timestamps_buf, SNAPSHOT_BUFFER_DIM_RIGHT);

  bot_pose_t bot_pose;
  initialize_bot_pose(&bot_pose);

  speed_control_data_t speed_control_data_left = {
    .error_buffer = {0},
    .iteration_count = 0,
    .target_speed = TARGET_SPEED,
    .current_speed = 0.0f,
    .control_action = 0.0f,
    .kp = KP_LEFT,
    .ki = KI_LEFT,
    .kd = KD_LEFT,
  };
  buffer_init(&speed_control_data_left.error_buffer, ERROR_BUFFER_DIM);

  speed_control_data_t speed_control_data_right = {
    .error_buffer = {0},
    .iteration_count = 0,
    .target_speed = TARGET_SPEED,
    .current_speed = 0.0f,
    .control_action = 0.0f,
    .kp = KP_RIGHT,
    .ki = KI_RIGHT,
    .kd = KD_RIGHT,
  };
  buffer_init(&speed_control_data_right.error_buffer, ERROR_BUFFER_DIM);

  heading_control_data_t heading_control_data = {
    .error_buffer = {0},
    .kp = KP_HEADING,
    .ki = KI_HEADING,
    .control_action_left = 0.0f,
    .control_action_right = 0.0f,
  };
  buffer_init(&heading_control_data.error_buffer, ERROR_BUFFER_DIM);
\end{lstlisting}

Le variabili locali vengono inizializzate.

\begin{lstlisting}[name=ticks_task,firstnumber=last]
  for (;;) {
#ifdef LOG_WCET
    uint32_t start_time = gpioTick();
#endif
    if (pthread_mutex_trylock(&history_mutex) != 0) {
      if (errno == EBUSY) {
        // The mutex was busy, yielding...
        sched_yield();
      } else {
        printf("Error: %d", errno);
        perror("odo_task: pthread_mutex_trylock");
        exit(EXIT_FAILURE);
      }
      continue;
    }
    // CRITICAL SECTION
    memcpy(history_left_local.ticks_buf.buffer,
           history_left.ticks_buf.buffer,
           sizeof(float) * SNAPSHOT_BUFFER_DIM_LEFT);
    memcpy(history_left_local.timestamps_buf.buffer,
           history_left.timestamps_buf.buffer,
           sizeof(float) * SNAPSHOT_BUFFER_DIM_LEFT);
    memcpy(history_right_local.ticks_buf.buffer,
           history_right.ticks_buf.buffer,
           sizeof(float) * SNAPSHOT_BUFFER_DIM_RIGHT);
    memcpy(history_right_local.timestamps_buf.buffer,
           history_right.timestamps_buf.buffer,
           sizeof(float) * SNAPSHOT_BUFFER_DIM_RIGHT);
    ASSERT(pthread_mutex_unlock(&history_mutex) != 0,
           "odo_task: pthread_mutex_unlock")
    // END CRITICAL SECTION
\end{lstlisting}

Nella sezione critica, per minimizzare il tempo per cui l'altro task è bloccato, si copiano i dati d'interesse in degli struct locali.

\begin{lstlisting}[name=ticks_task,firstnumber=last]
    update_speed(&history_left_local, &speed_control_data_left);
    update_speed(&history_right_local, &speed_control_data_right);

    rototranslation_calc(&bot_pose,
                          buffer_top(&history_left_local.ticks_buf),
                          buffer_top(&history_right_local.ticks_buf));
\end{lstlisting}

Vengono poi calcolate le velocità e la \textit{pose}, che contiene posizione nel piano e direzione.

\begin{lstlisting}[name=ticks_task,firstnumber=last]
    control_speed(&speed_control_data_left);
    control_speed(&speed_control_data_right);

    control_heading(&heading_control_data, &bot_pose);
\end{lstlisting}

Con queste informazioni si effettua il controllo

\begin{lstlisting}[name=ticks_task,firstnumber=last]
    move(&motor_left, &speed_control_data_left,
         heading_control_data.control_action_left);
    move(&motor_right, &speed_control_data_right,
         heading_control_data.control_action_right);
\end{lstlisting}
che viene poi azionato.

\begin{lstlisting}[name=ticks_task,firstnumber=last]
#ifdef LOG_DATA
    if (data_idx < LOG_DATA_SIZE) {
      sp_left[data_idx] = speed_control_data_left.current_speed +
                          heading_control_data.control_action_left;
      sp_right[data_idx] = speed_control_data_right.current_speed +
                            heading_control_data.control_action_right;
      dist_left[data_idx] =
          ticks2mm(buffer_top(&history_left_local.ticks_buf));
      dist_right[data_idx] =
          ticks2mm(buffer_top(&history_right_local.ticks_buf));
      ca_left[data_idx] = speed_control_data_left.control_action;
      ca_right[data_idx] = speed_control_data_right.control_action;
      times[data_idx] = gpioTick();
      data_idx++;
    }
#endif
#ifdef LOG_WCET
    uint32_t end_time = gpioTick();
    if (odo_wcet_idx < WCET_LOG_SIZE) {
      odo_wcet[odo_wcet_idx] = end_time - start_time;
      odo_wcet_idx++;
    }
#endif
\end{lstlisting}

Dopodichè vengono loggati i dati d'interesse.

\begin{lstlisting}[name=ticks_task,firstnumber=last]
    if (terminate_flag) {
#ifdef LOG_DATA
      print_path(&bot_pose, "odo.csv");
#endif
      break;
    }
    sched_yield();
  }
\end{lstlisting}

Se la \lstinline|terminate_flag| è impostata vuole dire che l'utente vuole la terminazione del programma, quindi, dopo aver eventualmente stampato informazioni sulla pose, usciamo dal ciclo.

\begin{lstlisting}[name=ticks_task,firstnumber=last]
  buffer_free(&history_left_local.ticks_buf);
  buffer_free(&history_left_local.timestamps_buf);
  buffer_free(&history_right_local.ticks_buf);
  buffer_free(&history_right_local.timestamps_buf);
  buffer_free(&speed_control_data_left.error_buffer);
  buffer_free(&speed_control_data_right.error_buffer);
  buffer_free(&heading_control_data.error_buffer);
  pthread_exit(NULL);
  return NULL;
}
\end{lstlisting}

Vengono rilasciate le risorse allocate precedentemente prima della fine della funzione.

\subsection{Interrupt Service Routines}

Per l'acquisizione dei dati relativi alla posizione angolare delle ruote si è fatto utilizzo di due funzioni, contenute in \lstinline|ISRs.c|.

\begin{lstlisting}[title={Snippet da \lstinline|ISRs.c|}]
void encoder_ISR_a(int gpio, int level, uint32_t timestamp, void* userdata) {
  cbEncoder_t* enc = (cbEncoder_t*)userdata;
  if (enc->last_gpio == gpio) return;
  enc->last_gpio = gpio;
  enc->level_a = level;
  if (enc->level_a ^ enc->level_b) {
    enc->ticks++;
    *(uint32_t*)enc->custom = timestamp;
  }
}

void encoder_ISR_b(int gpio, int level, uint32_t timestamp, void* userdata) {
  cbEncoder_t* enc = (cbEncoder_t*)userdata;
  if (enc->last_gpio == gpio) return;
  enc->last_gpio = gpio;
  enc->level_b = level;
  if (enc->level_a ^ enc->level_b) {
    enc->ticks--;
    *(uint32_t*)enc->custom = timestamp;
  }
}
\end{lstlisting}

Queste due funzioni operano sui due canali degli encoder, tenendo i dati necessari nella struttura fornite da \lstinline|libcoderbot|. Per prima cosa si effettua un semplice debouncing, se due o più interrupt consecutivi vengono dallo stesso canale, tutti quelli successivi al primo vengono ignorati. Questo serve a trascurare il rumore nel segnale, dato che il funzionamento corretto prevede l'alternarsi degli interrupt ai due canali. Poi si memorizza il \lstinline|gpio| dell'interrupt e il suo livello e, se la transizione è `valida', si incrementano/decrementano i tick e si memorizza il tempo di arrivo dell'interrupt.

Data la natura degli encoder è molto probabile che siano soggetti a letture spurie, ad esempio un decremento mentre la ruota sta andando in avanti o un incremento mentre va indietro, tuttavia, dato che la frequenza di sampling di questi dati appena raccolti è molto minore della frequenza usuale degli interrupt, questo non è risultato essere un problema.

\subsection{PID Controller}

Abbiamo utilizzato 3 controller: due PID che controllano la velocità delle ruote singolarmente e un PI che fa un controllo incrociato attraverso l'heading del bot.

Per calcolare le velocità delle ruote usiamo la funzione \lstinline|compute_speed|

\begin{lstlisting}[title=Snippet da \lstinline|pid.c|]
float compute_speed(const encoder_history_t* snapshot) {
  float speed_pos = 0.0f;
  int count_pos = 0;
  float speed_neg = 0.0f;
  int count_neg = 0;
  for (int i = 0; i < snapshot->ticks_buf.dim - 1; i++) {
    float num = ticks2mm(buffer_at(&snapshot->ticks_buf, i) -
                         buffer_at(&snapshot->ticks_buf, i + 1));
    float den = (buffer_at(&snapshot->timestamps_buf, i) -
                 buffer_at(&snapshot->timestamps_buf, i + 1)) /
                 S_TO_MICRO_MULTIPLIER;
    // se il denominatore e' nullo vuol dire che stiamo prendendo i dati
    // piu' in fretta di quanto ce ne siano, oppure che la ruota e' ferma
    if (den == 0.0f) {
      // consideriamo la velocita nulla
      // speed = 0.0f;
      // aumentiamo entrambi i counter (potrebbe capitare sia in avanti
      // che indietro)
      count_pos++;
      count_neg++;
      // e poi continuiamo (fare speed_pos += 0.0f sarebbe superfluo)
      continue;
    }
    float speed = num / den;
    if (isnan(speed) || isinf(speed) || fabs(speed) > 300.0f) {
      continue;
    }
    if (speed > 0.0f) {
      count_pos++;
      speed_pos += speed;
    } else {
      count_neg++;
      speed_neg += speed;
    }
  }
  if (count_pos > count_neg)
    return speed_pos / count_pos;
  else
    return speed_neg / count_neg;
}
\end{lstlisting}
Come è possibile osservare questa funzione non è una semplice media sulle ultime \lstinline|n| velocità, bensì fa due medie separate sulle velocità positive e negative, dove i valori nulli contribuiscono a entrambe, e restituisce la media con più contributi. Questo è stato necessario perché gli encoder alle volte restituiscono dei valori inconsistenti, necessitando di un pesante filtraggio. Allo stesso fine si ignorano tutti i valori invalidi, infiniti o maggiori delle velocità che ci si aspetti dal coderbot, che al massimo riesce ad arrivare a circa $200\flatfrac{\text{mm}}{\text{s}}$.

Dopo aver calcolato la velocità si passa ad aggiornare la \lstinline|bot_pose| attraverso la funzione \\ \lstinline|rototranslation_calc| che, come suggerisce il nome, calcola la rototraslazione del coderbot nel piano.

A questo punto abbiamo tutto il necessario per fare il controllo.\ \lstinline|control_speed| calcola l'errore come \lstinline|target_speed - current_speed|, che va poi accumulato in un buffer per l'errore integrale e derivato. % chktex 8
Analogamente si esegue il controllo per l'heading, in cui l'errore è \lstinline|right_error = target_heading - bot_heading|. L'errore destro e quello sinistro sono fortemente correlati,\\\lstinline|left_error = -right_error|. % chktex 8

I buffer dei tick, dei timestamp e degli errori sono stati dimensionati empiricamente, così come le costanti dei PID e PI.\@

\newpage

\section{Conclusioni}

Il progetto ha richiesto circa tre settimane di lavoro, al termine delle quali abbiamo ottenuto dei risultati soddisfacenti. Infatti il coderbot riesce a seguire la traiettoria piuttosto accuratamente. Il metodo di determinazione dei parametri lascia piuttosto a desiderare, dato che il tempo richiesto tra compilazione ed esecuzione è piuttosto lungo e i dati ottenuti non sono ovvi da interpretare.

Sebbene in principio si siano presentati molti problemi relativi agli encoder, necessitando delle forti manipolazioni dei dati letti, dopo l'introduzione di una differente frequenza di sampling questi si sono ridotti notevolmente. Infatti nelle prime fasi di progettazione i dati venivano scritti dalle ISRs direttamente nei buffer usati dalla routine di controllo, dando una bassissima latenza, ma una alta suscettibilità al rumore. Partire subito con una sampling frequency fissa e maggiore a quella degli interrupt avrebbe semplificato e velocizzato il resto del progetto.

Molto poco frequentemente succede che, quando si va a tentare di ottenere il lock del mutex, esso restituisca apparentemente un errore, ma quando interrogato attraverso la funzione \lstinline|perror| quello che viene restituito è un semplice \lstinline|"Success"|. Questo comportamento poco standard, combinato con la bassa probabilità che avvenga, ha reso il problema molto difficile da diagnosticare. % chktex 18

Durante i vari test non è mai stata mancata alcuna deadline, il che combacia con la garanzia della schedulabilità.

\end{document}