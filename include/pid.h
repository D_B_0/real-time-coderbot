#pragma once

#include <stdint.h>

#include "odometry.h"
#include "ring_buffer.h"

#define TARGET_SPEED 50.0f   // mm/s
#define TARGET_HEADING 0.0f  // gradi

#define KP_LEFT 0.000075f
#define KI_LEFT 0.000002f
#define KD_LEFT 0.0000025f

#define KP_RIGHT 0.000075f
#define KI_RIGHT 0.00001f
#define KD_RIGHT 0.00005f

#define KP_HEADING 0.002f
#define KI_HEADING 0.0025f

#define SNAPSHOT_BUFFER_DIM_LEFT 32
#define SNAPSHOT_BUFFER_DIM_RIGHT 32
#define ERROR_BUFFER_DIM 4

typedef struct speed_control_data {
    mBuffer_t error_buffer;
    uint32_t iteration_count;
    float target_speed;
    float current_speed;
    float control_action;
    float kp, ki, kd;
} speed_control_data_t;

typedef struct heading_control_data {
    mBuffer_t error_buffer;
    float kp, ki;
    float control_action_left, control_action_right;
} heading_control_data_t;

typedef struct encoder_history {
    mBuffer_t ticks_buf;
    mBuffer_t timestamps_buf;
} encoder_history_t;

float clamp(float control_action, float min, float max);
void update_speed(const encoder_history_t* snapshot,
                  speed_control_data_t* data);
void control_speed(speed_control_data_t* data);
void control_heading(heading_control_data_t* heading_data,
                     const bot_pose_t* bot_pose);