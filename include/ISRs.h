#pragma once

#include <stdint.h>

void encoder_ISR_a(int gpio, int level, uint32_t tick, void* userdata);
void encoder_ISR_b(int gpio, int level, uint32_t tick, void* userdata);