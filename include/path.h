/*
Libreria per gestire il path

COME UTILIZZARLA:

    - Impostare adeguatamente le costanti (SPLIT_LENGTH conviene lsciarla a 4)

    - Inizializzare l'odometria

    - Inizializzare lo struct con:
        path_struct p_str;
        (&p_str)->key_points[1][0] = 80.0f;
        (&p_str)->key_points[1][1] = 100.0f;
        (&p_str)->key_points[2][0] = 160.0f;
        (&p_str)->key_points[2][1] = 100.0f;
        ...
        initialize_path(&p_str, &pose_str);

    - Ad ogni iterazione del controllo chiamare la funzione update_goal:
        float error = 0.0f;
        update_goal(&p_str, &error);

    - L'errore è su y/x, e può essere usato per curvare insieme al campo (&p_str)->radius.
        Se questo è 0.0f, vuol dire che il raggio è infinito.

    - Alla fine del programma, potete stampare le posizioni raggiunte nel path:
        print_expected_path(&p_str, "path.txt");

*/

#include <math.h>
#include "odometry.h"

// ---------- CONSTANTS ---------- //

#define PATH_LENGTH 3
#define SPLIT_LENGTH 4
#define TOTAL_POINTS 100
#define ERROR_THRESH 50



// ---------- STRUCTS ---------- //

// Struct per tenere traccia del robot sul path
// Il primo key_point DEVE essere {0, 0}
// Verrà sovrascritto se non lo è
typedef struct _path_struct {
    float key_points[PATH_LENGTH][2]; // Forniti dall'esterno
    int next_goal; // Puntatore al prossimo key_point da raggiungere
    float split_points[TOTAL_POINTS][2]; // Punti
    int next_point; // Puntatore al prossimo punto da raggiungere
    bot_pose* odometry; // Puntatore all'odometria del robot
    float radius; // Valore del raggio della curva (se 0 ignorare)
    bool end;
} path_struct;



// ---------- UTILS ---------- //

// Calcola la posizione del nuovo goal rispetto a quella del robot
void calculate_relative_position(float new_point[2], float old_point[2], path_struct* str) {
    float x = old_point[0];
    float y = old_point[1];

    // Rappresenta il prossimo key_point rispetto alla pose corrente
    float tmp[3][3] = {{1, 0, x}, {0, 1, y}, {0, 0, 1}};
    float translated[2];
    translated[0] = x - str->odometry->current_pose[0][2];
    translated[1] = y - str->odometry->current_pose[1][2];
    float rotated[2];
    rotated[0] = translated[0] * str->odometry->current_pose[0][0] + translated[1] * str->odometry->current_pose[1][0];
    rotated[1] = translated[0] * str->odometry->current_pose[0][1] + translated[1] * str->odometry->current_pose[1][1];

    new_point[0] = rotated[0];
    new_point[1] = rotated[1];
}

// Riporta i punti splittati al sistema mondo
void back_to_world(path_struct* str) {
    for (int i = 0; i < SPLIT_LENGTH - 2; i++) {
        float x = str->split_points[str->next_point + i][0];
        float y = str->split_points[str->next_point + i][1];

        // Rappresenta il prossimo key_point rispetto alla pose corrente
        float RTMatrix[3][3];
        float tmp[3][3] = {{1, 0, x}, {0, 1, y}, {0, 0, 1}};
        multiply_matrix(RTMatrix, str->odometry->current_pose, tmp);

        str->split_points[str->next_point + i][0] = RTMatrix[0][2];
        str->split_points[str->next_point + i][1] = RTMatrix[1][2];
    }
    // Sistemo il primo e l'ultimo
    str->split_points[str->next_point - 1][0] = str->odometry->translation[0];
    str->split_points[str->next_point - 1][1] = str->odometry->translation[1];

    str->split_points[str->next_point + SPLIT_LENGTH-2][0] = str->key_points[str->next_goal][0];
    str->split_points[str->next_point + SPLIT_LENGTH-2][1] = str->key_points[str->next_goal][1];
}

// Divide la curva
void split_curve(path_struct* str) {
    str->next_point ++;
    float new_point[2];
    calculate_relative_position(new_point, str->key_points[str->next_goal], str);

    float interval = new_point[1] / (SPLIT_LENGTH - 1);
    // Se la linea è retta
    if (float_equal(new_point[1], 0.0f)) {
        interval = new_point[0] / (SPLIT_LENGTH - 1);
        str->radius = 0; // Metto questo a 0
        for (int i = 1; i < SPLIT_LENGTH - 1; i++) {
            str->split_points[str->next_point + i - 1][1] = 0.0f;
            str->split_points[str->next_point + i - 1][0] = i * interval;
        }
    } else {
        // Calcola il raggio del cerchio (sull'asse y <-)
        str->radius = (pow(new_point[0], 2) + pow(new_point[1], 2)) / (2*new_point[1]);

        // Quantizza su y (POI DOVRO' SETTARE IL PRIMO E L'ULTIMO)
        for (int i = 1; i < SPLIT_LENGTH - 1; i++) {
            str->split_points[str->next_point + i - 1][1] = i * interval;
            str->split_points[str->next_point + i - 1][0] = sqrt(pow(str->radius, 2) - pow(str->radius - i * interval, 2));
        }
    }

    // Riporto i punti nel sistema mondo
    back_to_world(str);
}

// Controlla se il robot ha raggiunto il goal
// Ritorna l'errore sulle y
void check_goal_reached(path_struct* str, bool* reached, float error) {
    // Trasforma le coordinate del goal rispetto al body
    float goal[2];
    calculate_relative_position(goal, str->split_points[str->next_point], str);
    
    // Controlla se il goal è stato superato
    if (goal[0] < 0) {
        // Guarda se si è allontanato troppo
        if (fabs(goal[1]) > ERROR_THRESH) {
            split_curve(str);
            calculate_relative_position(goal, str->split_points[str->next_point], str);
        } else {
            *reached = true;
        }
    }

    // Restituisce l'errore
    error = goal[1] / goal[0];
}

// Aggiorna il puntatori del path
void update_pointers(path_struct* str) {
    // Aggiorna il puntatore del path splittato
    str->next_point ++;

    // Se si ha superato un key_point, splitta il path
    if (str->split_points[str->next_point - 1][0] == str->key_points[str->next_goal][0]
    && str->split_points[str->next_point - 1][1] == str->key_points[str->next_goal][1]) {
        str->next_goal ++;

        // Se si ha raggiunto la fine termino
        if (str->next_goal == PATH_LENGTH) {
            printf("LAST GOAL REACHED!!!\n");
            str->end = true;

            // Mette nell'ultima posizione la pose del robot
            str->split_points[str->next_point - 1][0] = str->odometry->translation[0];
            str->split_points[str->next_point - 1][1] = str->odometry->translation[1];
        } else {
            split_curve(str);
        }
    }
}



// ---------- FUNCTIONS ---------- //

// Inizializza il path
void initialize_path(path_struct* str, bot_pose* odom) {
    str->end = false;

    // Setta i puntatori
    str->next_goal = 1;
    str->next_point = 1;

    // Setta il puntatore all'odometria
    str->odometry = odom;

    // Pone all'origine il primo punto del path
    str->key_points[0][0] = 0;
    str->key_points[0][1] = 0;

    // Divide il primo tratto di curva
    split_curve(str);
}

// Aggiorna il goal e restituisce l'errore sulle y
void update_goal(path_struct* str, float* error) {
    // Controlla se il goal corrente è stato raggiunto
    bool reached = false;
    float err = 0.0f;
    check_goal_reached(str, &reached, err);
    *error = err;

    // Se è stato raggiunto, aggiorna i puntatori
    while (reached && !str->end) {
        update_pointers(str);
        reached = false;
        check_goal_reached(str, &reached, err);
    }
}

// Stampa il path
void print_expected_path(path_struct* str, char filename[]) {
    FILE *f = fopen(filename, "w");
    for (int i = 0; i < str->next_point; i++) {
        float x = str->split_points[i][0];
        float y = str->split_points[i][1];
        
        fprintf(f, "x: %f    y: %f\n", x, y);
    }
    fclose(f);
}