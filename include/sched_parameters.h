#pragma once

#include <stdint.h>

#include "constants.h"

#define TICKS_WCET 10
#define ODO_WCET 80

#define TICKS_RUNTIME TICKS_WCET* NSEC_PER_MSEC  //< Expected task runtime in ns
#define TICKS_PERIOD 2 * TICKS_WCET* NSEC_PER_MSEC  //< Task period in ns
#define TICKS_DEADLINE \
    (2 * TICKS_WCET - 1) * NSEC_PER_MSEC  //< Task deadline in ns

#define ODO_RUNTIME ODO_WCET* NSEC_PER_MSEC     //< Expected task runtime in ns
#define ODO_PERIOD 2 * ODO_WCET* NSEC_PER_MSEC  //< Task period in ns
#define ODO_DEADLINE (2 * ODO_WCET - 1) * NSEC_PER_MSEC  //< Task deadline in ns

typedef struct sched_attr {
    uint32_t size;           /* Size of this structure */
    uint32_t sched_policy;   /* Policy (SCHED_*) */
    uint64_t sched_flags;    /* Flags */
    int32_t sched_nice;      /* Nice value (SCHED_OTHER, SCHED_BATCH) */
    uint32_t sched_priority; /* Static priority (SCHED_FIFO, SCHED_RR) */
    /* The following fields are for SCHED_DEADLINE */
    uint64_t sched_runtime;
    uint64_t sched_deadline;
    uint64_t sched_period;
} sched_attr_t;