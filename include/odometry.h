#pragma once
/*
Libreria di funzioni e struttura per il calcolo dell'odometria del coderbot

COME UTILIZZARLA:

    - Impostare adeguatamente le costanti (POSE_MEM_LENGTH conviene metterla a
un valore leggermente maggiore rispetto al numero di iterazioni del controllo)

    - Inizializzare lo struct con:
        bot_pose pose_str;
        initialize_bot_pose(&pose_str);

    - Ad ogni iterazione del controllo chiamare la funzione
rototranslation_calc: rototranslation_calc(&pose_str, tot_tick_sx, tot_tick_dx);

    - Alla fine del programma, potete stampare le posizioni raggiunte coi
relativi tick: print_path(&pose_str, "nome_file.txt");
*/

// ---------- CONSTANTS ---------- //
#include <stdbool.h>

#define POSE_MEM_LENGTH 1000
#define BASELINE 118.0f  // mm

// ---------- STRUCTS ---------- //

// Struct per tenere traccia delle pose
// Queste sono segnate come delle matrici di rototraslazione omogenee rispetto
// all'origine Per semplicità, scompone la pose corrente anche in angolo e
// traslazione
typedef struct bot_pose {
    float alpha;  // Angolo in gradi
    float translation[2];
    float current_pose[3][3];
    float past_poses[POSE_MEM_LENGTH][3][3];
    int pose_count;
    float tot_distance_dx;
    float tot_distance_sx;
    int tick_array[POSE_MEM_LENGTH][2];
} bot_pose_t;

// ---------- UTILS ---------- //

// Compara due float
bool float_equal(float a, float b);

// Riempie una matrice 3x3 di cui ha un puntatore
// I valori sono dati come tre vettori colonna
void fill_matrix(float matrix[3][3], float c1[3], float c2[3], float c3[3]);

// Calcola l'angolo theta e il raggio h del cerchio osculatore
// Mette i parametri dentro l'array passato
void calculate_params(float params[2], float ssx, float sdx);

// Moltiplica due matrici
void multiply_matrix(float result[3][3], float first[3][3], float second[3][3]);

// Calcola la distanza dai tick
float ticks2mm(int ticks);

// ---------- FUNCTIONS ---------- //

// Fa l'update dello struct
void update_pose(bot_pose_t* bot, float matrix[3][3]);

// Inizializza lo struct delle pose
void initialize_bot_pose(bot_pose_t* bot);

// Crea la nuova rototraslazione dai tick correnti
void rototranslation_calc(bot_pose_t* bot, int tsx, int tdx);

// Stampa su file lo storico delle pose
void print_path(bot_pose_t* bot, char filename[]);
