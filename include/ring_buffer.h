#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int dim;
    float* buffer;
    int index;
} mBuffer_t;

void buffer_init(mBuffer_t* buf, int dim);
void buffer_free(mBuffer_t* buf);
void buffer_push(mBuffer_t* buf, float val);
float buffer_top(const mBuffer_t* buf);
float buffer_bottom(const mBuffer_t* buf);
float buffer_at(const mBuffer_t* buf, int i);
void buffer_print(const mBuffer_t* buf);
