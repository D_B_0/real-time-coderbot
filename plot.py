#!/bin/python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

run = pd.read_csv("run.csv")
odo = pd.read_csv("odo.csv")
odo_wcet = pd.read_csv("odo_wcet.csv")
ticks_wcet = pd.read_csv("ticks_wcet.csv")

run["times"] += np.where(run["times"] < 0, np.max(run["times"]) - np.min(run["times"]), 0)
run["times"] -= np.min(run["times"])
run["times"] /= 1_000_000

ax_left = plt.subplot(1, 2, 1)
ax_right = plt.subplot(1, 2, 2, sharey=ax_left)

ca_left_plot = ax_left.plot(run["times"], run["ca_left"], color="red", marker="+", linestyle="", label="control action")
ax_left_twin = ax_left.twinx()
sp_left_plot = ax_left_twin.plot(run["times"], run["sp_left"], color="blue", marker=".", linestyle="", label="speed")

ca_right_plot = ax_right.plot(run["times"], run["ca_right"], color="red", marker="+", linestyle="", label="control action")
ax_right_twin = ax_right.twinx()
sp_right_plot = ax_right_twin.plot(run["times"], run["sp_right"], color="blue", marker=".", linestyle="", label="speed")

ax_left.legend(ca_left_plot + sp_left_plot, [l.get_label() for l in ca_left_plot + sp_left_plot])
ax_right.legend(ca_right_plot + sp_right_plot, [l.get_label() for l in ca_right_plot + sp_right_plot])
ax_left.set_title("Left wheel speed and control action")
ax_right.set_title("Right wheel speed and control action")
plt.show()

if True:
	plt.plot(run["times"], run["sp_right"], color="blue", marker=".", linestyle="", label="right")
	plt.plot(run["times"], run["sp_left"], color="red", marker=".", linestyle="", label="left")
	plt.legend()
	plt.show()

if True:
	sorted_odo = np.sort(odo_wcet["odo_wcet"])
	sorted_ticks = np.sort(ticks_wcet["ticks_wcet"])
	print("odo worst calc 10 times  ", sorted_odo[-10:])
	print("odo mean calc time       ", np.mean(sorted_odo))
	print("odo median calc time     ", np.median(sorted_odo))
	print("ticks worst calc 10 times", sorted_ticks[-10:])
	print("ticks mean calc time     ", np.mean(sorted_ticks))
	print("ticks median calc time   ", np.median(sorted_ticks))
	fig, axs = plt.subplots(1, 2, tight_layout=True)
	axs[0].hist(odo_wcet["odo_wcet"], bins=np.max(odo_wcet["odo_wcet"]) - np.min(odo_wcet["odo_wcet"]), label="odo")
	axs[1].hist(ticks_wcet["ticks_wcet"], bins=np.max(ticks_wcet["ticks_wcet"]) - np.min(ticks_wcet["ticks_wcet"]), label="ticks")
	axs[0].set_title("odo task computation times histogram")
	axs[0].legend()
	axs[1].set_title("ticks task computation times histogram")
	axs[1].legend()
	plt.show()
	fig, axs = plt.subplots(1, 2, tight_layout=True)
	axs[0].plot(odo_wcet["odo_wcet"], linestyle="", marker=".", label="odo")
	axs[1].plot(ticks_wcet["ticks_wcet"], linestyle="", marker=".", label="ticks")
	axs[0].set_title("odo task computation times graph")
	axs[0].legend()
	axs[1].set_title("ticks task computation times graph")
	axs[1].legend()
	plt.show()

plt.axis('equal')
plt.plot(odo["x"], odo["y"])
plt.grid(which="both", axis="both")
plt.title("Path on the plane")
plt.show()

plt.plot(odo["heading"])
plt.show()

# plt.plot(run["times"][:-1], (run["sp_left"] - run["sp_right"])[:-1] * np.diff(run["times"]))
# plt.show()

# print(np.sum(run["sp_left"][:-1] * np.diff(run["times"])))
# print(np.sum(run["sp_right"][:-1] * np.diff(run["times"])))
# print(np.sum((run["sp_left"] - run["sp_right"])[:-1] * np.diff(run["times"])))
