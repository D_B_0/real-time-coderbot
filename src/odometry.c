
#include "../include/odometry.h"

#include <math.h>
#include <stdio.h>

#include "../include/constants.h"
#include "../include/wheel_constants.h"

bool float_equal(float a, float b) { return (fabs(a - b) < 0.001f); }

// Riempie una matrice 3x3 di cui ha un puntatore
// I valori sono dati come tre vettori colonna
void fill_matrix(float matrix[3][3], float c1[3], float c2[3], float c3[3]) {
    for (int i = 0; i < 3; i++) {
        matrix[i][0] = c1[i];
        matrix[i][1] = c2[i];
        matrix[i][2] = c3[i];
    }
}

// Calcola l'angolo theta e il raggio h del cerchio osculatore
// Mette i parametri dentro l'array passato
void calculate_params(float params[2], float ssx, float sdx) {
    params[0] = 0.0f;  // h
    params[1] = 0.0f;  // theta
    if (!float_equal(ssx, sdx)) {
        float d = (BASELINE * sdx) / (ssx - sdx);
        params[1] = (ssx - sdx) / BASELINE;
        params[0] = d + BASELINE / 2;
    }
}

// Moltiplica due matrici
void multiply_matrix(float result[3][3], float first[3][3],
                     float second[3][3]) {
    // Inizializzo matrice risultato
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            result[i][j] = 0.0f;
        }
    }
    // Moltiplico le due matrici
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            for (int k = 0; k < 3; k++) {
                result[i][j] += first[i][k] * second[k][j];
            }
        }
    }
}

// Calcola la distanza dai tick
float ticks2mm(int ticks) {
    return ticks * WHEEL_RADIUS * 2.0 * M_PI /
           (TRANSMISSION_RATIO * TICKS_PER_REVOLUTION);
}

// ---------- FUNCTIONS ---------- //

// Fa l'update dello struct
void update_pose(bot_pose_t* bot, float matrix[3][3]) {
    float i[3] = {matrix[0][0], matrix[1][0], matrix[2][0]};
    float j[3] = {matrix[0][1], matrix[1][1], matrix[2][1]};
    float t[3] = {matrix[0][2], matrix[1][2], matrix[2][2]};

    // Aggiorna la pose corrente
    fill_matrix(bot->current_pose, i, j, t);

    if (bot->pose_count < POSE_MEM_LENGTH) {
        // Aggiunge la pose a quelle passate
        fill_matrix((bot->past_poses)[bot->pose_count], i, j, t);

        // Incrementa il counter
        bot->pose_count++;
    }

    // Aggiorna la traslazione
    bot->translation[0] = bot->current_pose[0][2];
    bot->translation[1] = bot->current_pose[1][2];

    // Aggiorna l'angolo alpha in gradi
    float alpha_rad = atan2(bot->current_pose[1][0], bot->current_pose[0][0]);
    bot->alpha = (alpha_rad * 180.0f) / M_PI;
}

// Inizializza lo struct delle pose
void initialize_bot_pose(bot_pose_t* bot) {
    // Imposta il pose_count e le distanze a 0
    bot->pose_count = 0;
    bot->tot_distance_dx = 0.0f;
    bot->tot_distance_sx = 0.0f;
    bot->tick_array[0][0] = 0;
    bot->tick_array[0][1] = 0;

    // Imposta la pose corrente all'origine del mondo
    float mat[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
    update_pose(bot, mat);
}

// Crea la nuova rototraslazione dai tick correnti
void rototranslation_calc(bot_pose_t* bot, int tsx, int tdx) {
    // Calcola l'ultimo tratto
    float ssx = ticks2mm(tsx) - bot->tot_distance_sx;
    float sdx = ticks2mm(tdx) - bot->tot_distance_dx;
    // Aggiorna distanza totale percorsa
    bot->tot_distance_sx += ssx;
    bot->tot_distance_dx += sdx;
    bot->tick_array[bot->pose_count][0] = tsx;
    bot->tick_array[bot->pose_count][1] = tdx;

    // Recupera i parametri
    float params[2];  // h, theta
    calculate_params(params, ssx, sdx);
    float h = params[0];
    float theta = params[1];

    // Calcola le matrici
    float RTMatrix[3][3] = {0};
    if (float_equal(theta, 0.0f)) {  // Se theta = 0
        float i[] = {1.0f, 0.0f, 0.0f};
        float j[] = {0.0f, 1.0f, 0.0f};
        float t[] = {ssx, 0.0f, 1.0f};
        fill_matrix(RTMatrix, i, j, t);
    } else {
        float tmpMatrix[3][3] = {0};  // Per le moltiplicazioni
        float RT1_C1[3][3] = {
            {1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, -h}, {0.0f, 0.0f, 1.0f}};
        float RC1_C2[3][3] = {{cos(-theta), -sin(-theta), 0.0f},
                              {sin(-theta), cos(-theta), 0.0f},
                              {0.0f, 0.0f, 1.0f}};
        float RC1_T[3][3] = {
            {1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, h}, {0.0f, 0.0f, 1.0f}};
        multiply_matrix(tmpMatrix, RT1_C1, RC1_C2);
        multiply_matrix(RTMatrix, tmpMatrix, RC1_T);
    }
    float final_matrix[3][3];
    multiply_matrix(final_matrix, bot->current_pose, RTMatrix);

    // Fa l'update
    update_pose(bot, final_matrix);
}

// Stampa su file lo storico delle pose
void print_path(bot_pose_t* bot, char filename[]) {
    FILE* f = fopen(filename, "w");
    fputs("x,y,heading,tsx,trx,dtsx,dtrx\n", f);
    for (int i = 0; i < bot->pose_count; i++) {
        float x = bot->past_poses[i][0][2];
        float y = bot->past_poses[i][1][2];
        float heading =
            atan2(bot->past_poses[i][1][0], bot->past_poses[i][0][0]) * 180.0f /
            M_PI;
        float ts = bot->tick_array[i][0];
        float td = bot->tick_array[i][1];
        float diff_dx = 0;
        float diff_sx = 0;
        if (i > 0) {
            diff_dx = bot->tick_array[i][1] - bot->tick_array[i - 1][1];
            diff_sx = bot->tick_array[i][0] - bot->tick_array[i - 1][0];
        }

        fprintf(f, "%f,%f,%f,", x, y, heading);
        fprintf(f, "%f,%f,", ts, td);
        fprintf(f, "%f,%f\n", diff_sx, diff_dx);
    }
    fclose(f);
}
