#include "../include/ISRs.h"

#include "../../include/encoder.h"

void encoder_ISR_a(int gpio, int level, uint32_t timestamp, void* userdata) {
    cbEncoder_t* enc = (cbEncoder_t*)userdata;
    if (enc->last_gpio == gpio) return;
    enc->last_gpio = gpio;
    enc->level_a = level;
    if (enc->level_a ^ enc->level_b) {
        enc->ticks++;
        *(uint32_t*)enc->custom = timestamp;
    }
}

void encoder_ISR_b(int gpio, int level, uint32_t timestamp, void* userdata) {
    cbEncoder_t* enc = (cbEncoder_t*)userdata;
    if (enc->last_gpio == gpio) return;
    enc->last_gpio = gpio;
    enc->level_b = level;
    if (enc->level_a ^ enc->level_b) {
        enc->ticks--;
        *(uint32_t*)enc->custom = timestamp;
    }
}