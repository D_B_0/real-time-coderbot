#include "../include/ring_buffer.h"

void buffer_init(mBuffer_t* buf, int dim) {
    buf->dim = dim;
    buf->index = 0;
    buf->buffer = (float*)malloc(dim * sizeof(float));
    memset(buf->buffer, 0.0f, dim * sizeof(float));
}

void buffer_free(mBuffer_t* buf) {
    buf->dim = 0;
    buf->index = 0;
    free((void*)buf->buffer);
}

void buffer_push(mBuffer_t* buf, float val) {
    buf->index = (buf->index + 1) % buf->dim;
    buf->buffer[buf->index] = val;
}

float buffer_top(const mBuffer_t* buf) { return buf->buffer[buf->index]; }

float buffer_bottom(const mBuffer_t* buf) {
    return buf->buffer[(buf->index + 1) % buf->dim];
}

float buffer_at(const mBuffer_t* buf, int i) {
    return buf->buffer[(buf->index + buf->dim - i) % buf->dim];
}

void buffer_print(const mBuffer_t* buf) {
    for (int i = 0; i < buf->dim; i++) {
        printf("%d - %f\n", i, buffer_at(buf, i));
    }
}
