#include "../include/pid.h"

#include <math.h>

#include "../include/constants.h"

inline float clamp(float t, float min, float max) {
    if (t > max) return max;
    if (t < min) return min;
    return t;
}

float compute_speed(const encoder_history_t* snapshot) {
    float speed_pos = 0.0f;
    int count_pos = 0;
    float speed_neg = 0.0f;
    int count_neg = 0;
    for (int i = 0; i < snapshot->ticks_buf.dim - 1; i++) {
        float num = ticks2mm(buffer_at(&snapshot->ticks_buf, i) -
                             buffer_at(&snapshot->ticks_buf, i + 1));
        float den = (buffer_at(&snapshot->timestamps_buf, i) -
                     buffer_at(&snapshot->timestamps_buf, i + 1)) /
                    S_TO_MICRO_MULTIPLIER;
        // se il denominatore e' nullo vuol dire che stiamo prendendo i dati
        // piu' in fretta di quanto ce ne siano, oppure che la ruota e' ferma
        if (den == 0.0f) {
            // consideriamo la velocita nulla
            // speed = 0.0f;
            // aumentiamo entrambi i counter (potrebbe capitare sia in avanti
            // che indietro)
            count_pos++;
            count_neg++;
            // e poi continuiamo (fare speed_pos += 0.0f sarebbe superfluo)
            continue;
        }
        float speed = num / den;
        if (isnan(speed) || isinf(speed) || fabs(speed) > 300.0f) {
            continue;
        }
        if (speed > 0.0f) {
            count_pos++;
            speed_pos += speed;
        } else {
            count_neg++;
            speed_neg += speed;
        }
    }
    if (count_pos > count_neg)
        return speed_pos / count_pos;
    else
        return speed_neg / count_neg;
}

void update_speed(const encoder_history_t* snapshot,
                  speed_control_data_t* data) {
    data->current_speed = compute_speed(snapshot);
    if (isnan(data->current_speed)) data->current_speed = 0.0f;
}

void control_speed(speed_control_data_t* data) {
    float error = data->target_speed - data->current_speed;
    buffer_push(&data->error_buffer, error);
    float integral_error = 0.0f;
    for (int i = 0; i < data->error_buffer.dim; i++) {
        integral_error += buffer_at(&data->error_buffer, i);
    }
    integral_error /= data->error_buffer.dim;
    float derivative_error =
        buffer_at(&data->error_buffer, 0) - buffer_at(&data->error_buffer, 1);

    data->control_action += (error * data->kp) + (integral_error * data->ki) +
                            (derivative_error * data->kd);
}

void control_heading(heading_control_data_t* heading_data,
                     const bot_pose_t* bot_pose) {
    float right_error = TARGET_HEADING - bot_pose->alpha;
    float left_error = -right_error;

    buffer_push(&heading_data->error_buffer, right_error);

    float right_integral_error = 0.0f;
    for (int i = 0; i < heading_data->error_buffer.dim; i++) {
        right_integral_error += buffer_at(&heading_data->error_buffer, i);
    }
    right_integral_error /= heading_data->error_buffer.dim;
    float left_integral_error = -right_integral_error;

    heading_data->control_action_left =
        left_error * heading_data->kp + left_integral_error * heading_data->ki;
    heading_data->control_action_right =
        right_error * heading_data->kp +
        right_integral_error * heading_data->ki;
}
