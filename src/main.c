#include <errno.h>
#include <linux/sched.h>
#include <pigpio.h>
#include <signal.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>

#include "../../include/cbdef.h"
#include "../../include/encoder.h"
#include "../../include/motor.h"
#include "../include/ISRs.h"
#include "../include/pid.h"
#include "../include/sched_parameters.h"

// Per stimare il wcet va definito LOG_WCET.
// Alla terminazione del programma, verrano creati due file
// con le durate dei task
#define LOG_WCET
// Analogamente, per avere il log dei dati serve questo define
#define LOG_DATA

#define ASSERT(condition, message) \
    if (condition) {               \
        perror(message);           \
        exit(EXIT_FAILURE);        \
    }

void move(cbMotor_t* mot, speed_control_data_t* speed_data,
          float heading_control_action);

pthread_mutex_t history_mutex;
encoder_history_t history_left = {
    .ticks_buf = {0},
    .timestamps_buf = {0},
};
encoder_history_t history_right = {
    .ticks_buf = {0},
    .timestamps_buf = {0},
};

cbMotor_t motor_left = {.pin_fw = PIN_LEFT_FORWARD,
                        .pin_bw = PIN_LEFT_BACKWARD,
                        .direction = forward};
cbMotor_t motor_right = {.pin_fw = PIN_RIGHT_FORWARD,
                         .pin_bw = PIN_RIGHT_BACKWARD,
                         .direction = forward};

uint32_t timestamp_left = 0, timestamp_right = 0;

cbEncoder_t enc_left = {
    .pin_a = PIN_ENCODER_LEFT_A,
    .pin_b = PIN_ENCODER_LEFT_B,
    .last_gpio = GPIO_PIN_NC,
    .level_a = 0,
    .level_b = 0,
    .direction = 0,
    .ticks = 0,
    .bad_ticks = 0,
    .custom = &timestamp_left,
};
cbEncoder_t enc_right = {
    .pin_a = PIN_ENCODER_RIGHT_A,
    .pin_b = PIN_ENCODER_RIGHT_B,
    .last_gpio = GPIO_PIN_NC,
    .level_a = 0,
    .level_b = 0,
    .direction = 0,
    .ticks = 0,
    .bad_ticks = 0,
    .custom = &timestamp_right,
};

#ifdef LOG_DATA
#define LOG_DATA_SIZE 2000
float sp_left[LOG_DATA_SIZE];
float ca_left[LOG_DATA_SIZE];
float dist_left[LOG_DATA_SIZE];
float dist_right[LOG_DATA_SIZE];
float sp_right[LOG_DATA_SIZE];
float ca_right[LOG_DATA_SIZE];
uint32_t times[LOG_DATA_SIZE];
int data_idx = 0;
#endif

#ifdef LOG_WCET
#define WCET_LOG_SIZE 2000
uint32_t odo_wcet[WCET_LOG_SIZE];
int odo_wcet_idx = 0;
uint32_t ticks_wcet[WCET_LOG_SIZE];
int ticks_wcet_idx = 0;
#endif

static inline int sched_setattr(pid_t pid, const pthread_attr_t* attr,
                                unsigned int flags) {
    // TODO who knows what "flags" does? Is
    // it the same as "sched_attr.flags"?
    return syscall(__NR_sched_setattr, pid, attr, flags);
}

void deadline_missed_handler(int _sig) {
    puts("Deadline miss!\n");
    gpioTerminate();
    (void)signal(SIGXCPU, SIG_DFL);
}

int terminate_flag = 0;

void ctrl_c_handler(int _sig) {
    if (terminate_flag != 0) {
        fputs("control c pressed again, killing program\n", stderr);
        exit(EXIT_FAILURE);
    }
    fputs("control c pressed!\n", stderr);
    terminate_flag = 1;
}

void* ticks_task(void* _arg) {
    sched_attr_t attr = {.size = sizeof(attr),
                         .sched_flags = 0 | SCHED_FLAG_DL_OVERRUN,
                         .sched_policy = SCHED_DEADLINE,
                         .sched_runtime = TICKS_RUNTIME,     // ns
                         .sched_period = TICKS_PERIOD,       // ns
                         .sched_deadline = TICKS_DEADLINE};  // ns
    // Register signal handler
    (void)signal(SIGXCPU, deadline_missed_handler);
    (void)signal(SIGINT, ctrl_c_handler);
    (void)signal(SIGTERM, ctrl_c_handler);
    (void)signal(SIGKILL, ctrl_c_handler);
    ASSERT(sched_setattr(0, &attr, 0) != 0, "ticks_task: sched_setattr")
    for (;;) {
#ifdef LOG_WCET
        uint32_t start_time = gpioTick();
#endif
        int res = pthread_mutex_trylock(&history_mutex);
        if (res != 0) {
            if (errno == EBUSY) {
                // The mutex was busy, yielding...
                sched_yield();
            } else {
                perror("ticks_task: pthread_mutex_trylock");
                printf("pthread_mutex_trylock return value: %d\n", res);
                exit(EXIT_FAILURE);
            }
            continue;
        }
        buffer_push(&history_left.ticks_buf, (float)enc_left.ticks);
        buffer_push(&history_left.timestamps_buf, (float)timestamp_left);
        buffer_push(&history_right.ticks_buf, (float)enc_right.ticks);
        buffer_push(&history_right.timestamps_buf, (float)timestamp_right);
        ASSERT(pthread_mutex_unlock(&history_mutex) != 0,
               "ticks_task: pthread_mutex_unlock")
#ifdef LOG_WCET
        uint32_t end_time = gpioTick();
        if (ticks_wcet_idx < WCET_LOG_SIZE) {
            ticks_wcet[ticks_wcet_idx] = end_time - start_time;
            ticks_wcet_idx++;
        }
#endif
        pthread_testcancel();
        sched_yield();
    }
    pthread_exit(NULL);
    return NULL;
}

void* odo_task(void* _arg) {
    sched_attr_t attr = {.size = sizeof(attr),
                         .sched_flags = 0 | SCHED_FLAG_DL_OVERRUN,
                         .sched_policy = SCHED_DEADLINE,
                         .sched_runtime = ODO_RUNTIME,     // ns
                         .sched_period = ODO_PERIOD,       // ns
                         .sched_deadline = ODO_DEADLINE};  // ns
    // Register signal handlers
    (void)signal(SIGXCPU, deadline_missed_handler);
    (void)signal(SIGINT, ctrl_c_handler);
    (void)signal(SIGTERM, ctrl_c_handler);
    (void)signal(SIGKILL, ctrl_c_handler);

    ASSERT(sched_setattr(0, &attr, 0) != 0, "odo_task: sched_setattr")

    encoder_history_t history_left_local = {0};
    buffer_init(&history_left_local.ticks_buf, SNAPSHOT_BUFFER_DIM_LEFT);
    buffer_init(&history_left_local.timestamps_buf, SNAPSHOT_BUFFER_DIM_LEFT);

    encoder_history_t history_right_local = {0};
    buffer_init(&history_right_local.ticks_buf, SNAPSHOT_BUFFER_DIM_RIGHT);
    buffer_init(&history_right_local.timestamps_buf, SNAPSHOT_BUFFER_DIM_RIGHT);

    bot_pose_t bot_pose;
    initialize_bot_pose(&bot_pose);

    speed_control_data_t speed_control_data_left = {
        .error_buffer = {0},
        .iteration_count = 0,
        .target_speed = TARGET_SPEED,
        .current_speed = 0.0f,
        .control_action = 0.0f,
        .kp = KP_LEFT,
        .ki = KI_LEFT,
        .kd = KD_LEFT,
    };
    buffer_init(&speed_control_data_left.error_buffer, ERROR_BUFFER_DIM);

    speed_control_data_t speed_control_data_right = {
        .error_buffer = {0},
        .iteration_count = 0,
        .target_speed = TARGET_SPEED,
        .current_speed = 0.0f,
        .control_action = 0.0f,
        .kp = KP_RIGHT,
        .ki = KI_RIGHT,
        .kd = KD_RIGHT,
    };
    buffer_init(&speed_control_data_right.error_buffer, ERROR_BUFFER_DIM);

    heading_control_data_t heading_control_data = {
        .error_buffer = {0},
        .kp = KP_HEADING,
        .ki = KI_HEADING,
        .control_action_left = 0.0f,
        .control_action_right = 0.0f,
    };
    buffer_init(&heading_control_data.error_buffer, ERROR_BUFFER_DIM);

    for (;;) {
#ifdef LOG_WCET
        uint32_t start_time = gpioTick();
#endif
        if (pthread_mutex_trylock(&history_mutex) != 0) {
            if (errno == EBUSY) {
                // The mutex was busy, yielding...
                sched_yield();
            } else {
                printf("Error: %d", errno);
                perror("odo_task: pthread_mutex_trylock");
                exit(EXIT_FAILURE);
            }
            continue;
        }
        // CRITICAL SECTION
        memcpy(history_left_local.ticks_buf.buffer,
               history_left.ticks_buf.buffer,
               sizeof(float) * SNAPSHOT_BUFFER_DIM_LEFT);
        memcpy(history_left_local.timestamps_buf.buffer,
               history_left.timestamps_buf.buffer,
               sizeof(float) * SNAPSHOT_BUFFER_DIM_LEFT);
        memcpy(history_right_local.ticks_buf.buffer,
               history_right.ticks_buf.buffer,
               sizeof(float) * SNAPSHOT_BUFFER_DIM_RIGHT);
        memcpy(history_right_local.timestamps_buf.buffer,
               history_right.timestamps_buf.buffer,
               sizeof(float) * SNAPSHOT_BUFFER_DIM_RIGHT);
        ASSERT(pthread_mutex_unlock(&history_mutex) != 0,
               "odo_task: pthread_mutex_unlock")
        // END CRITICAL SECTION
        update_speed(&history_left_local, &speed_control_data_left);
        update_speed(&history_right_local, &speed_control_data_right);

        rototranslation_calc(&bot_pose,
                             buffer_top(&history_left_local.ticks_buf),
                             buffer_top(&history_right_local.ticks_buf));

        control_speed(&speed_control_data_left);
        control_speed(&speed_control_data_right);

        control_heading(&heading_control_data, &bot_pose);

        move(&motor_left, &speed_control_data_left,
             heading_control_data.control_action_left);
        move(&motor_right, &speed_control_data_right,
             heading_control_data.control_action_right);

#ifdef LOG_DATA
        if (data_idx < LOG_DATA_SIZE) {
            sp_left[data_idx] = speed_control_data_left.current_speed +
                                heading_control_data.control_action_left;
            sp_right[data_idx] = speed_control_data_right.current_speed +
                                 heading_control_data.control_action_right;
            dist_left[data_idx] =
                ticks2mm(buffer_top(&history_left_local.ticks_buf));
            dist_right[data_idx] =
                ticks2mm(buffer_top(&history_right_local.ticks_buf));
            ca_left[data_idx] = speed_control_data_left.control_action;
            ca_right[data_idx] = speed_control_data_right.control_action;
            times[data_idx] = gpioTick();
            data_idx++;
        }
#endif
#ifdef LOG_WCET
        uint32_t end_time = gpioTick();
        if (odo_wcet_idx < WCET_LOG_SIZE) {
            odo_wcet[odo_wcet_idx] = end_time - start_time;
            odo_wcet_idx++;
        }
#endif
        if (terminate_flag) {
#ifdef LOG_DATA
            print_path(&bot_pose, "odo.csv");
#endif
            break;
        }
        sched_yield();
    }
    buffer_free(&history_left_local.ticks_buf);
    buffer_free(&history_left_local.timestamps_buf);
    buffer_free(&history_right_local.ticks_buf);
    buffer_free(&history_right_local.timestamps_buf);
    buffer_free(&speed_control_data_left.error_buffer);
    buffer_free(&speed_control_data_right.error_buffer);
    buffer_free(&heading_control_data.error_buffer);
    pthread_exit(NULL);
    return NULL;
}

void move(cbMotor_t* mot, speed_control_data_t* speed_data,
          float heading_control_action) {
    speed_data->control_action = clamp(speed_data->control_action, 0.0f, 1.0f);
    if (speed_data->iteration_count < 25) {
        speed_data->control_action =
            clamp(speed_data->control_action, 0.0f, 0.5f);
    }
    speed_data->iteration_count++;
    float control_action =
        clamp(speed_data->control_action + heading_control_action, 0.0f, 1.0f);
    cbMotorMove(mot, forward, control_action);
}

void init() {
    if (gpioInitialise() < 0) exit(EXIT_FAILURE);

    // Left
    cbEncoderGPIOinit(&enc_left);
    cbEncoderRegisterCustomISRs(&enc_left, EITHER_EDGE, encoder_ISR_a,
                                EITHER_EDGE, encoder_ISR_b, 50);
    // Right
    cbEncoderGPIOinit(&enc_right);
    cbEncoderRegisterCustomISRs(&enc_right, EITHER_EDGE, encoder_ISR_a,
                                EITHER_EDGE, encoder_ISR_b, 50);

    cbMotorGPIOinit(&motor_left);
    cbMotorGPIOinit(&motor_right);

    buffer_init(&history_left.ticks_buf, SNAPSHOT_BUFFER_DIM_LEFT);
    buffer_init(&history_left.timestamps_buf, SNAPSHOT_BUFFER_DIM_LEFT);

    buffer_init(&history_right.ticks_buf, SNAPSHOT_BUFFER_DIM_RIGHT);
    buffer_init(&history_right.timestamps_buf, SNAPSHOT_BUFFER_DIM_RIGHT);

    pthread_mutexattr_t priority_inheritance;
    ASSERT(pthread_mutexattr_init(&priority_inheritance) != 0 ||
               pthread_mutexattr_setprotocol(&priority_inheritance,
                                             PTHREAD_PRIO_INHERIT),
           "main: pthread_mutexattr_init")
    ASSERT(pthread_mutex_init(&history_mutex, &priority_inheritance) != 0,
           "main: pthread_mutex_init")

    ASSERT(pthread_mutexattr_destroy(&priority_inheritance) != 0,
           "main: pthread_mutexattr_destroy")
}

void terminate() {
    cbEncoderCancelISRs(&enc_left);
    cbEncoderCancelISRs(&enc_right);

    cbMotorReset(&motor_left);
    cbMotorReset(&motor_right);
    gpioTerminate();

    buffer_free(&history_left.ticks_buf);
    buffer_free(&history_left.timestamps_buf);

    buffer_free(&history_right.ticks_buf);
    buffer_free(&history_right.timestamps_buf);

    ASSERT(pthread_mutex_destroy(&history_mutex) != 0,
           "main: pthread_mutex_destroy")

#ifdef LOG_WCET
    FILE* odo_wcet_log_file = fopen("odo_wcet.csv", "w");
    ASSERT(odo_wcet_log_file == NULL, "terminate: fopen")
    fputs("odo_wcet\n", odo_wcet_log_file);
    for (int i = 0; i < odo_wcet_idx; i++) {
        fprintf(odo_wcet_log_file, "%d\n", odo_wcet[i]);
    }
    fclose(odo_wcet_log_file);

    FILE* ticks_wcet_log_file = fopen("ticks_wcet.csv", "w");
    ASSERT(ticks_wcet_log_file == NULL, "terminate: fopen")
    fputs("ticks_wcet\n", ticks_wcet_log_file);
    for (int i = 0; i < ticks_wcet_idx; i++) {
        fprintf(ticks_wcet_log_file, "%d\n", ticks_wcet[i]);
    }
    fclose(ticks_wcet_log_file);
#endif
#ifdef LOG_DATA
    FILE* log_file = fopen("run.csv", "w");
    ASSERT(log_file == NULL, "terminate: fopen")
    fputs("sp_left,sp_right,dist_left,dist_right,ca_left,ca_right,times\n",
          log_file);
    for (int i = 0; i < data_idx; i++) {
        fprintf(log_file, "%f,%f,%f,%f,%f,%f,%d\n", sp_left[i], sp_right[i],
                dist_left[i], dist_right[i], ca_left[i], ca_right[i], times[i]);
    }
    fclose(log_file);
#endif
    exit(0);
}

int main(void) {
    init();
    atexit(terminate);

    pthread_t ticks_tid, odo_tid;
    ASSERT(pthread_create(&ticks_tid, NULL, ticks_task, NULL) != 0,
           "main: pthread_create")
    ASSERT(pthread_create(&odo_tid, NULL, odo_task, NULL) != 0,
           "main: pthread_create")

    ASSERT(pthread_join(odo_tid, NULL) != 0, "main: pthread_join")
    fputs("main: odo_task: Completed!\n", stderr);
    ASSERT(pthread_cancel(ticks_tid) != 0, "main: pthread_cancel")
    ASSERT(pthread_join(ticks_tid, NULL) != 0, "main: pthread_join")
    fputs("main: ticks_task: Completed!\n", stderr);

    exit(EXIT_SUCCESS);
}
