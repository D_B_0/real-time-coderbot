ARCH = $(shell uname -m)

SRC := $(wildcard src/*.c)
EXE := main.$(ARCH)

CFLAGS := -std=gnu99 -Wall -pedantic -Iinclude
LDFLAGS := -L..
LDLIBS := -lpigpio -lpthread -l:libcoderbot.a -lm

DEBUG ?= 1
ifeq (DEBUG, 1)
    CFLAGS += -g -O0 -Wall -Werror -Wextra -DDEBUG
else
	CFLAGS += -O2 -march=native -DNDEBUG
endif

.PHONY: all clean

all: $(EXE)

$(EXE): $(SRC)
	$(CC) $(CFLAGS) $(SRC) -o $(EXE) $(LDFLAGS) $(LDLIBS)

clean:
	$(RM) $(EXE)